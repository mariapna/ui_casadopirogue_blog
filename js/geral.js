$(function(){


	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	//CARROSSEL DE DESTAQUE
	$("#carrosselDestaque").owlCarousel({
		items : 2,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
        responsive:{
            320:{
                items:1
            },
            768:{
                items:1
            },
            991:{
                items:2
            },
            1024:{
                items:2
            },
            1440:{
                items:2
            },

        }

	});

	var userFeed = new Instafeed({
       get: 'user',
       userId: '9474199589',
       clientId: '3ee792d09dc041cf93c67398c9cdc06b',
       accessToken: '9474199589.3ee792d.248483f575994c78a96eba151bce62a9',
       resolution: 'standard_resolution',
       template: '<a href="{{link}}" target="_blank" id="{{id}}"><div class="itemInstagram" style="background:url({{image}})"><small class="likeComments"><span class="likes">{{likes}}</span><span class="comments">{{comments}}</span></small></div></a>',
       sortBy: 'most-recent',
       limit: 9,
       links: false
     });
     userFeed.run();
		
});